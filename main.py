"""URL Shortener Hack"""
import json
import os
import shutil

def analytics(short, full):
    html = f"""<script src="https://getinsights.io/js/insights.js"></script> <script>insights.init('{os.environ['TRACK_ID']}');insights.trackPages(); insights.track({{id: "go", parameters:{{short: '{short}', full: '{full}',}}}});</script>"""
    return html

def main():
    """Main Function"""
    html = """<html><head>{analytics}<meta http-equiv="refresh" content="1;url={url}" /></noscript></head></html>"""

    with open('links.json') as f:
        links = json.load(f)

    shutil.rmtree('dist', ignore_errors=True)
    os.mkdir('dist')

    with open('dist/CNAME', 'w') as f:
        f.write('antb.fun')

    for link in links:
        html_document = html.format(url=link['url'], analytics=analytics(link['name'], link['url']))
        file_path = f"dist/{link['name']}.html"

        with open(file_path, 'w') as f:
            f.write(html_document)


if __name__ == "__main__":
    main()
